package bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.Cookie;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import DAO.UserDAO;
import utils.CookieUtils;

@ManagedBean(name="homeView")
@ViewScoped
public class HomeView {

	@ManagedProperty(value="#{user}")
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	private List<User> usersList;
	

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}
	
	CookieUtils cookieUtils;

	public CookieUtils getCookieUtils() {
		return cookieUtils;
	}

	public void setCookieUtils(CookieUtils cookieUtils) {
		this.cookieUtils = cookieUtils;
	}
	
	private UserDAO userDao;

	@PostConstruct
	public void init() {
		
		usersList = new ArrayList<User>();
		cookieUtils = new CookieUtils();
		
		userDao = new UserDAO();
		
		Cookie userCookie = cookieUtils.getCookie("loginToken") != null ? cookieUtils.getCookie("loginToken") : new Cookie("loginToken","");
		
		String userToken = userCookie.getValue();	
				
		try {
			usersList = userDao.getUsersList();
						
		} catch (Exception e) {
			error("Please contact the administrator for help");
			e.printStackTrace();
		}
	}
	
	public void addUser() {
		
		setUser(new User());
		
//		userClient.addUser(user);
		
	}
	
	public void saveNewUser() {
		
		System.out.println(user.getUserCode());
	}
	
	public void updateUser() {
		
	}
	
	public void timeManagement(ActionEvent event) {
    	
    	//TO-DO -> redirect to userManagement page
		FacesContext context = FacesContext.getCurrentInstance();
//    	context.getExternalContext().invalidateSession();
	    
    	try {
			context.getExternalContext().redirect("/timeManagement/home.xhtml");
		} catch (Exception e) {
			error("Please contact the administrator for help");
			e.printStackTrace();
		}
         
    }

	public void projectManagement(ActionEvent event) {
    	
		//TO-DO -> redirect to projectManagement page
    	FacesContext context = FacesContext.getCurrentInstance();
//    	context.getExternalContext().invalidateSession();
	    
    	try {
			context.getExternalContext().redirect("/timeManagement/pmView.xhtml");
		} catch (Exception e) {
			error("Please contact the administrator for help");
			e.printStackTrace();
		}
         
    }  
	
	public void userManagement(ActionEvent event) {
    	
    	//TO-DO -> redirect to userManagement page
		FacesContext context = FacesContext.getCurrentInstance();
//    	context.getExternalContext().invalidateSession();
	    
    	try {
			context.getExternalContext().redirect("/timeManagement/umView.xhtml");
		} catch (Exception e) {
			error("Please contact the administrator for help");
			e.printStackTrace();
		}
         
    }
	
	public void logout(ActionEvent event) {
    	
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.getExternalContext().invalidateSession();
	    
    	try {
			context.getExternalContext().redirect("/login/login.xhtml");
		} catch (Exception e) {
			error("Please contact the administrator for help");
			e.printStackTrace();
		}
         
    }
	
	public void info(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", message));
    }
 
    public void warn(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", message));
    }
 
    public void error(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", message));
    }
 
    public void fatal(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal!", message));
    }
}
