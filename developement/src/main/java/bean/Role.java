package bean;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIgnoreProperties(ignoreUnknown = true)	
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="roleCode", scope=Role.class)
public class Role {
	
	private String roleCode;
	private String roleName;
	
	private Set<User> usersList;
	
	public Role(String roleCode, String roleName, Set<User> usersList) {
		super();
		this.roleCode = roleCode;
		this.roleName = roleName;
		this.usersList = usersList;
	}

	public Role() {
		super();
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(Set<User> usersList) {
		this.usersList = usersList;
	}

}
