package rest.services;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("api")
public class UserManagementApp extends ResourceConfig {
    public UserManagementApp() {
        packages("rest.services");
    }
}