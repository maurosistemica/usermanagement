package rest.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import DAO.RoleDAO;
import bean.Role;
import bean.User;

@Path("/roles")
public class RoleService {
	
	RoleDAO roleDao = new RoleDAO();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRolesList() {
		
		List<Role> rolesList = new ArrayList<Role>();
				
		rolesList = roleDao.getRolesList();
 
		return Response.status(200).entity(rolesList).build();
		
	}

}
