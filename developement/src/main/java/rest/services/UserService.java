package rest.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import DAO.UserDAO;
import bean.User;

@Path("/users")
public class UserService {
	
	@Context
    private UriInfo uriInfo;
	
	UserDAO userDao = new UserDAO();
	
//	@Path("{userToken}")
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
////	@JWTTokenNeeded
//	public Response getUsersByProjectCodeAndUsername(@PathParam("userToken") @Encoded String userToken) {
//		
//		JWTUtils jwtUtils = new JWTUtils();
//		
//		User user = jwtUtils.parseJWT(userToken);
//
//		List<Project> projectList = userDao.getProjectsByUsername(user.getUserCode());
//		
//		JSONObject jsonObject = new JSONObject();
//		
//			if(CollectionUtils.isNotEmpty(projectList)) {
//				for(Project pjt : projectList) {
//					jsonObject.put(pjt.getProjectCode(),pjt.getProjectName());
//				}
//			}
//		
//		return Response.status(200).entity(jsonObject.toMap()).build();
//	}
	
//	@Path("{userCode}/roles")
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getRolesListByUserCode(@PathParam("userCode") String userCode) {
//		
//		List<Role> roleList = userDao.getRolesByUserCode(userCode);
//		
//		JSONObject jsonObject = new JSONObject();
//		
//		if(CollectionUtils.isNotEmpty(roleList)) {
//			for (Role role : roleList) {
//				jsonObject.put(role.getRoleCode(),userCode);
//			}
//		}
// 
//		return Response.status(200).entity(jsonObject.toMap()).build();
//		
//	}
	
	@Path("{userCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserByUserCode(@PathParam("userCode") String userCode) {
		
		User user = userDao.getUserByUserCode(userCode);
 
		return Response.status(200).entity(user).build();
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersList() {
		
		List<User> userList = new ArrayList<User>();
				
		userList = userDao.getUsersList();
		
//		JSONObject jsonObject = new JSONObject();
		
//		if(CollectionUtils.isNotEmpty(userList)) {
//			for (User user : userList) {
//				if(CollectionUtils.isNotEmpty(user.getRolesList())) {
//					for(Role role : user.getRolesList()) {
//						jsonObject.put(role.getRoleCode(),user.getUserCode());
//					}
//				}
//			}
//		}
 
		return Response.status(200).entity(userList).build();
		
	}
	
	@Path("{userCode}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response udpateUser(User user, @PathParam("userCode") String userCode) {
		
		Boolean alreadyExists = userDao.userExists(userCode);
		
		if(alreadyExists) {
			try {
				userDao.updateUser(user);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return Response.created(uriInfo.getAbsolutePath()).build();
			
		} else {		
			return Response.noContent().build();
			
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveUser(User user) {
		
		Boolean alreadyExists = userDao.userExists(user.getUserCode());
		
		if(!alreadyExists) {
			try {
				userDao.saveUser(user);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return Response.created(uriInfo.getAbsolutePath()).build();
			
		} else {		
			return Response.noContent().build();
			
		}
	}
	
	@Path("{userCode}")
	@DELETE
	public Response deleteUser(@PathParam("userCode") String userCode) {
		
		try {
			userDao.deleteUserByUserCode(userCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.created(uriInfo.getAbsolutePath()).build();
	}

}
