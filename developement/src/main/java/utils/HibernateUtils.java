package utils;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtils {
	
	public static final SessionFactory sessionFactory;

		static {
		    try {
		      // Create the SessionFactory from hibernate.cfg.xml
		    	 Configuration configuration = new Configuration();
		    	 configuration.configure();
		    	 ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
		    	            configuration.getProperties()).buildServiceRegistry();
		    	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		    } catch (Throwable ex) {
		      // Make sure you log the exception, as it might be swallowed
		      System.err.println("Initial SessionFactory creation failed." + ex);
		      throw new ExceptionInInitializerError(ex);
		    }
		}

	  public final ThreadLocal session = new ThreadLocal();

	  public Session currentSession() throws HibernateException {
	    Session s = (Session) session.get();
	    // Open a new Session, if this thread has none yet
	    if (s == null || !s.isOpen()) {
	      s = sessionFactory.openSession();
	      // Store it in the ThreadLocal variable
	      session.set(s);
	    }
	    return s;
	  }

	  public void closeSession() throws HibernateException {
	    Session s = (Session) session.get();
	    if (s != null)
	      s.close();
	    session.set(null);
	  }

}
