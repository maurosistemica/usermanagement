package DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bean.Role;
import utils.HibernateUtils;

public class RoleDAO {
	
	HibernateUtils hibernateUtils = new HibernateUtils();
	
	public List<Role> getRolesList() {
		
		List<Role> rolesList = new ArrayList<Role>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from Role");
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				rolesList = (List<Role>) query.list();
			} 
			
			tx.commit();
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return rolesList;
	}

}
