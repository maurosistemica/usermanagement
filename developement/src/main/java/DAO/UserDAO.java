package DAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bean.Role;
import bean.User;
import utils.HibernateUtils;

public class UserDAO {

	HibernateUtils hibernateUtils = new HibernateUtils();
	
	public List<User> getUsersList() {
		
		List<User> usersList = new ArrayList<User>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from User");
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				usersList = (List<User>) query.list();
			} 
			
			tx.commit();
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return usersList;
	}

//	public List<Role> getRolesByUserCode(String userCode) {
//		
//		List<Role> roleList = new ArrayList<Role>();
//		
//		Session session = null;		
//		Transaction tx = null;
//		
//		try{
//			
//			session = hibernateUtils.currentSession();
//
//			tx = session.beginTransaction();
//		
//			Query query = session.createQuery("select distinct roles from User u join u.roleList roles where u.userCode = :userCode");
//			query.setParameter("userCode", userCode);
//			
//			if(CollectionUtils.isNotEmpty(query.list())) {
//				roleList = (List<Role>) query.list();
//			} 
//			
//			tx.commit();
//			
//		} catch(Exception e) {
//			e.printStackTrace();
//		} finally {
//			session.close();
//		}
//		
//		return roleList;
//	}
	
	public List<User> getUsersByRoleCode(String roleCode) {
		
		List<User> userList = new ArrayList<User>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("select distinct u from User u join u.roleList roles where roles.roleCode = :roleCode");
			query.setParameter("roleCode", roleCode);
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				userList = (List<User>) query.list();
			} 
			
			tx.commit();
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return userList;
	}
	
	public User getUserByUserCode(String userCode) {
				
		User user = new User();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("select distinct u from User u join u.role r where upper(u.userCode) = :userCode");
			query.setParameter("userCode", userCode.toUpperCase());
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				user = (User) query.uniqueResult();
			} 
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			e.printStackTrace();
		} finally {
			session.close();
		}
				
		return user;
		
	}
	
	public Boolean userExists(String userCode) {
		
		Boolean alreadyExists = false;
		
		List<User> userList = new ArrayList<User>();
		
		Session session = null;		
		Transaction tx = null;
		
		try{
			
			session = hibernateUtils.currentSession();

			tx = session.beginTransaction();
		
			Query query = session.createQuery("from User u where upper(u.userCode) = :userCode");
			query.setParameter("userCode", userCode.toUpperCase());
			
			if(CollectionUtils.isNotEmpty(query.list())) {
				userList = (List<User>) query.list();
			} 
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		if(!CollectionUtils.isEmpty(userList)) {
			alreadyExists = true;
		}
		
		return alreadyExists;
	}
	
	public void updateUser(User user) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			session.update(user);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}
	
	public void saveUser(User user) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			session.save(user);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}
	
	public void deleteUserByUserCode(String userCode) throws Exception {
		
		Session session = hibernateUtils.currentSession();
		Transaction tx;
		
		try {
			tx = session.beginTransaction();
			
			User deletedUser = (User) session.load(User.class, userCode);
			
			session.delete(deletedUser);
			
			//aggiungere write tabella log
			
			tx.commit();
			
		} catch(Exception e) {
			session.flush();
			session.clear();
			throw e;
		} finally {
			session.clear();
			session.close();
		}		
	}
	
}
